import socket, ssl, errno
from threading import Thread, RLock
import os
import csv
import sys
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Util import Counter
from base64 import b64encode
from OpenSSL import crypto
import shutil

#pip3 install mysql-connector-python
import mysql.connector

# sudo apt-get install build-essential libffi-dev python-dev  for all dependacies
# pip3 install bcrypt
import bcrypt


class Server:
	def __init__(self, mysqlUser,mysqlPass,host = '', port = 1024):
		databaseOk = False
		try:
			self._database = DB(str(mysqlUser),str(mysqlPass))
			databaseOk = True
		except :
			print("Error connecting to the database. Please insert the right username and password.")
		if(databaseOk):
			self._host = host
			self._port = port
			self._macrosDictionnary = {}
			self._connections = {}
			self.getMacros()
			self._mutex = RLock()
			context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
			context.load_cert_chain('certificate.pem','private.key')
			socketS = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
			socketS.bind((self._host,self._port))
			self._ssocket = context.wrap_socket(socketS, server_side=True)
			self._ssocket.listen(5)
			self.listenForClient()

	def close(self):
		self._ssocket.close()

	def getMacros(self):
		with open('macrosFile.txt') as f:
			reader = csv.DictReader(f, delimiter = ':')
			for row in reader:
				self._macrosDictionnary[row['Code']] = row['Action']
	
	def getMacroValue(self, value):
		for code, action in self._macrosDictionnary.items():
				if action == value:
					return code

	def listenForClient(self):
		print("Waiting for client connection")
		connect = True
		while connect:
			try:
				dualConnection=False
				client,addr = self._ssocket.accept()
				if(addr[0] in self._connections):
					if(self._connections[addr[0]]>0):
						print("Blocked "+ str(addr[0])+ " because it has more than one connections")
						client.close()
						dualConnection=True
					else:
						self._connections[addr[0]] = 1
				else:
					self._connections[addr[0]] = 1					
				if(not dualConnection):
					if(client.recv(1024).decode()=="1"):
						if(client.recv(1024).decode()=="1"):
							print("Connection accepted from : " + str(addr[0]) + ": " + str(addr[1]))
							newClientHandlerThread = ClientHandler(client,addr,self._connections,self._database,self._mutex)
							newClientHandlerThread.daemon = True
							newClientHandlerThread.start()
			except KeyboardInterrupt:
				print("Server was forced to shut down")
				self.close()
				connect= False
			except:
				print("Couldn't connect to the client")


class DB:
	def __init__(self,user,passwd,host="localhost",db= "NasDB"):
		self._db = mysql.connector.connect(host = host,user= user,passwd=passwd,db=db,charset='utf8mb4')
		self._cursor = self._db.cursor() #Execute queries

	def userNameExist(self,username):
		self._cursor.execute("""SELECT Username FROM User WHERE Username=("%s")"""%(username))
		return str(self._cursor.fetchone())!="None"

	def userLogIn(self,username,password,secretPass= ""):
		if(secretPass==""):
			self._cursor.execute("""SELECT Password FROM User WHERE Username=("%s") AND Accepted=true """%(username))
		else:
			self._cursor.execute("""SELECT Password FROM User WHERE Username=("%s") AND SecretPass = ("%s") AND Accepted=true """%(username,secretPass))
		userPassword = self._cursor.fetchone()
		if(userPassword!=None):
			userPassword = userPassword[0].encode()
		else : 
			return False
		return bcrypt.hashpw(password.encode(), userPassword) == userPassword

	def isAdminUser(self,username) : 
		self._cursor.execute("""SELECT Administrator FROM User WHERE Username=("%s") AND Accepted=true """%(username))
		res = self._cursor.fetchone()
		if(res==None):
			return res
		else:
			return res[0]

	def addUser(self,username,password,SecretPass):
		self._cursor.execute("""INSERT INTO User(Username,Password,SecretPass,Accepted,Administrator) VALUES (("%s"),("%s"),("%s"),false,false)"""%(username,bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode(),SecretPass))

	def delUser(self,username):
		#Suppression d'un utilisateur
		self._cursor.execute("""DELETE FROM User WHERE Username=("%s") """%(username))
		self._cursor.execute("""DELETE FROM Rights WHERE Username= ("%s") """%(username))

	def delFile(self,filePath):
		#Suppression d'un fichier
		self._cursor.execute("""DELETE FROM File WHERE RealPath = ("%s") """%(filePath))
		self._cursor.execute("""DELETE FROM Rights WHERE FilePath = ("%s") """%(filePath))

	def userToAccept(self):
		#Tout les utilisateurs à accepter.
		self._cursor.execute("""SELECT Username FROM User WHERE Accepted=false""")
		res = self._cursor.fetchall()
		if(res!=None):
			listing = []
			for user in res : 
				listing.append(user[0])
			return listing
		else:
			return []

	def isGroupFile(self,filePath):
		self._cursor.execute("""SELECT Username FROM Rights WHERE FilePath=("%s") AND Agreements IS NOT NULL """%(filePath))
		res = self._cursor.fetchall()
		return res!=None

	def acceptUser(self,username):
		#Accepter les users. Ne peut que être lancé par les administrateurs.
		self._cursor.execute("""UPDATE User SET Accepted=true WHERE Username=("%s") """%(username))

	def disableUser(self,username):
		self._cursor.execute("""UPDATE User SET Accepted=false WHERE Username=("%s") """%(username))

	def putUserAdmin(self,username):
		#Faire devenir un simple User en Administrateur
		self._cursor.execute("""UPDATE User SET Accepted=true , Administrator=true WHERE Username=("%s") """%(username))

	def insertFileCreation(self,username,fileName,permissions,filePath) :
		#Insertion d'un fichier
		self._cursor.execute("""INSERT INTO File(Name,RealPath) VALUES (("%s"),("%s"))"""%(fileName,filePath))
		self._cursor.execute("""INSERT INTO Rights(Username,FilePath,rights) VALUES (("%s"),("%s"),("%s"))"""%(username,filePath,permissions))
	
	def fileAlreadyExists(self,filePath):
		#Vérifie que le fichier existe déjà ou pas.
		self._cursor.execute("""SELECT Name FROM File WHERE filePath = ("%s") """%(filePath))
		fileExistance = self._cursor.fetchone()
		if(fileExistance==None):
			return False
		else:
			return True

	def groupAgree(self,filePath):
		self._cursor.execute("""SELECT COUNT(*) FROM Rights WHERE FilePath=("%s") AND  Agreements = true"""%(filePath))
		resAgree = self._cursor.fetchone()
		agree = resAgree[0]
		self._cursor.execute("""SELECT COUNT(*) FROM Rights WHERE FilePath=("%s") AND  Agreements IS NOT NULL"""%(filePath))
		allRes = self._cursor.fetchone()
		allAgree = allRes[0]
		return int(allAgree)==int(agree)

	def putAgree(self,username,filePath):
		self._cursor.execute("""UPDATE Rights SET Agreements=true WHERE Username=("%s") AND FilePath=("%s") """%(username,filePath))

	def putDisagree(self,username,fileName):
		self._cursor.execute("""UPDATE Rights SET Agreements=false WHERE Username=("%s") AND FilePath=("%s") """%(username,fileName))


	def fileToSeeByUser(self,username):
		#On ne précise pas qu'elle genre d'action il peut faire sur le fichier !
		self._cursor.execute("""SELECT f.RealPath FROM File f, Rights r WHERE r.Username=("%s") AND r.FilePath = f.RealPath """%(username))
		res = self._cursor.fetchall()
		if(res!= None):
			listing = []
			for Name in res : 
				listing.append(Name[0])
			return listing
		else:
			return []

	def fileAuthoriseToModify(self,username):
		self._cursor.execute("""SELECT f.RealPath, r.rights, f.Name FROM File f, Rights r WHERE r.Username=("%s") AND r.FilePath = f.RealPath """%(username))
		res = self._cursor.fetchall()
		if(res!=None):
			listing = []
			for fileInfo in res : 
				if("w" in fileInfo[1]):
					listing.append(fileInfo[0])
			return listing
		else:
			return []

	def groupFileAuthoriseToRead(self,username):
		self._cursor.execute("""SELECT f.RealPath, r.rights, f.Name FROM File f, Rights r WHERE r.Username=("%s") AND r.FilePath = f.RealPath AND Agreements IS NOT NULL """%(username))
		res = self._cursor.fetchall()
		if(res!=None):
			listing = []
			for fileInfo in res : 
				if("r" in fileInfo[1]):
					listing.append(fileInfo[0])
			return listing
		else:
			return []

	def insertShareFile(self,username,filePath,permission,groupAgree):
		self._cursor.execute("""INSERT INTO Rights(Username,FilePath,rights,Agreements) VALUES(("%s"),("%s"),("%s"),("%s"))"""%(username,filePath,permission,groupAgree))

	def deleteRepertory(self,filePath):
		self._cursor.execute("""DELETE FROM Rights WHERE FilePath LIKE %s """,(filePath+"%",))
		self._cursor.execute("""DELETE FROM File WHERE RealPath LIKE %s """,(filePath+"%",))

	def commit(self):
		self._db.commit()


class ClientHandler(Thread):
	def __init__(self,client,addr,connections,db,mutex):
		self._client=client
		self._addr=addr
		self._connections = connections
		self._database=db
		self._username = ""
		self._mutex=mutex
		self._macrosDictionnary = {}
		self._listPathRecursive = []
		self.getMacros()
		privateKeyString = open("private.key","r").read()
		self._privateKeySignature = crypto.load_privatekey(crypto.FILETYPE_PEM, privateKeyString)
		privateKey = RSA.importKey(privateKeyString)
		cipherRSA = PKCS1_OAEP.new(privateKey)
		self._aesKey = cipherRSA.decrypt(client.recv(1024))
		Thread.__init__(self)

	def run(self):
		self.clientConnection()
		if(self._addr[0] in self._connections):
			self._connections[self._addr[0]]=0

	def send(self,data, file=False):
		self._client.send(self.encrypting(data))
		self._client.send(self.encrypting(self.sign(data)))

	def sign(self,data):
		sign = b64encode(crypto.sign(self._privateKeySignature,data,'sha256'))
		return sign
	
	def receive(self,file=False):
		if (not file):
			return self.decrypting(self._client.recv(1024)).decode('ascii')
		else:
			return self.decrypting(self._client.recv(16384))	

	def getMacros(self):
		with open('macrosFile.txt') as f:
			reader = csv.DictReader(f, delimiter = ':')
			for row in reader:
				self._macrosDictionnary[row['Code']] = row['Action']
	
	def getMacroValue(self, value):
		for code, action in self._macrosDictionnary.items():
				if action == value:
					return code		

	def encrypting(self,plainText):
		cipher = AES.new(key=self._aesKey, mode=AES.MODE_CTR, counter=Counter.new(AES.block_size * 8))
		return cipher.encrypt(plainText)

	def decrypting(self,cipherText):
		cipher = AES.new(key=self._aesKey, mode=AES.MODE_CTR, counter=Counter.new(AES.block_size * 8))
		return cipher.decrypt(cipherText)

	def clientConnectionLoginIn(self):
		print("client is login in")
		userOk= False
		count = 0
		secretPass = ""
		try:
			while not userOk:
				self._username = self.receive()
				password = self.receive()
				if(count>3):
					secretPass = self.receive()
				count+=1
				with self._mutex:
					if(not self._database.userLogIn(self._username,password,secretPass)):
						self.send("NOTOK")
					else : 
						self.send("OK")
						try:
							os.mkdir(self._username)
						except OSError as e:
							print("your directory already exists!")
						self.clientConnectionLogInMenu()
						userOk = True
		except socket.error as e:
			self.handleKeyboardInterrupt(e,True)

	def clientConnection(self):
		userOk = False
		try:
			while not userOk:
				userChoiceCode = self.receive()
				if (userChoiceCode in self._macrosDictionnary):
					userChoice = self._macrosDictionnary[userChoiceCode]
					if(userChoice=="SIGNUP"):
						self.clientConnectionSignUp() #Après ça, attendre qu'un administrateur accepte
					elif(userChoice=="LOGIN"): 
						self.clientConnectionLoginIn()


					elif(userChoice=="QUIT"):
						print("Connection aborted by : "+str(self._addr[0]) + ": " + str(self._addr[1]))
						userOk=True
				else:
					self.send("NOTOK")
		except socket.error as e:
			self.handleKeyboardInterrupt(e,False)

	def handleKeyboardInterrupt(self,e,goBack):
		if isinstance(e.args, tuple):
			if e.args[0] == errno.EPIPE:
				print("Detected remote disconnect : " + str(self._addr[0]) + ": " + str(self._addr[1]))
			else:
				print("Errno is %d" % e.args[0])
				print("Error description: %s" % e.args[1])
		else:
			print("Socket error ", e)
		if (not goBack):
			self._client.close()

	def clientConnectionSignUp(self):
		userOk = False
		while not userOk:
			try:
				self._username = self.receive()
				if(self._username!=""):
					print("sign up----receive username")
				with self._mutex:
					if(not self._database.userNameExist(self._username)):
						self.send("OK")
						userOk = True
					else : 
						self.send("NOTOK")
				password = self.receive()
				if(password!=""):
					print("sign up----receive password")
				SecretPass = self.receive()
				if(SecretPass!=""):
					print("sign up----receive answerLife")
				with self._mutex:
					if(len(self._username.strip(" "))>0):
						self._database.addUser(self._username,password,SecretPass)
						self._database.commit()
				if(self._username!="" and password!=""):
					print("sign up terminé, user enregistré")
			except socket.error as e:
				userOk=True
				self.handleKeyboardInterrupt(e,True)

	def deleteFile(self,fileToDelete):
		if (os.path.exists(fileToDelete)):
			if (os.path.isdir(fileToDelete)):
				shutil.rmtree(fileToDelete)
				self._database.deleteRepertory(fileToDelete)
			else:
				os.remove(fileToDelete)	
				self._database.delFile(fileToDelete)
			self._database.commit()

	def clientConnectionLogInMenu(self):
		self.send(str(self._database.isAdminUser(self._username)))
		userOk=False
		try:
			while not userOk:
				request = self.receive()
				if (request in self._macrosDictionnary):
					if (self._macrosDictionnary[request] == "BROWSEFILES"):
						print("sending files for this client")
						allFilesUser = self._database.fileToSeeByUser(self._username)
						print(allFilesUser)
						for i in allFilesUser:
							self.send(i)
						self.send("FINISHED")	
					elif(self._macrosDictionnary[request] == "UPLOADFILE"):
						print("the client request to upload a file")
						tmp=self.receive()
						if (tmp=="DIRECTORY"):
							print("le client upload initiallement un repertoire")
							self.receiveDirectory()
							print("Finished transfer directory after recursif")

						elif(tmp=="NOTDIRECTORY"):	
							print("le client upload initiallement un fichier")
							self.receiveFile(self._username,True)	
						self._database.commit()	

					elif(self._macrosDictionnary[request] == "DELETEFILE"):
						print("the client request to delete a file")
						allFilesUser = self._database.fileAuthoriseToModify(self._username)
						allFilesUser += self._database.groupFileAuthoriseToRead(self._username)
						for fileFound in allFilesUser:
							self.send(fileFound)
						self.send("FINISHED")
						fileToDelete = self.receive()
						if(not self._database.isGroupFile(fileToDelete)):
							self.deleteFile(fileToDelete)
						else:
							self._database.putAgree(self._username,fileToDelete)
							self._database.commit()
							if(self._database.groupAgree(fileToDelete)):
								self.deleteFile(fileToDelete)
						print("Finish to delete file")

					elif(self._macrosDictionnary[request] == "CREATEDIRECTORY"):
						print("the client request to create a directory")
						allFilesUser = self._database.fileAuthoriseToModify(self._username)
						for fileFound in allFilesUser:
							if (os.path.isdir(fileFound)):
								self.send(fileFound)
						self.send("FINISHED")
						directoryPath = self.receive()
						directoryName = self.receive()
						self._database.insertFileCreation(self._username,directoryName,"w",directoryPath+"/"+directoryName)
						try:
							os.mkdir(directoryPath+"/"+directoryName)
						except OSError as e:
							print("your directory already exists!")
						self._database.commit()

					elif(self._macrosDictionnary[request] == "DOWNLOADFILE"):
						print("the client request to download a file")
						allFilesUser = self._database.fileToSeeByUser(self._username)
						print(allFilesUser)
						for i in allFilesUser:
							self.send(i)
						self.send("FINISHED")
						fileToDownload = self.receive()#filename
						fileName = self.getFileName(fileToDownload)
						if (os.path.exists(fileToDownload)):
							if (os.path.isdir(fileToDownload)):
								self.send("DIRECTORY")
								print("sent DIRECTORY====")
								self._listPathRecursive.append(fileName)
								self.sendDirectory(fileName, fileToDownload)#fullpath is the path we get from db function
								print("finished sendDirectory recursif")
								self.send("NOTCONTINUE")
								print("sent NOTCONTINUE===")
							else:
								self.send("NOTDIRECTORY")
								self.sendFile(fileName, fileToDownload, True)

					elif(self._macrosDictionnary[request] == "LOGOUT"):
						print("the client requested to log out!")
						userOk = True

					elif(self._macrosDictionnary[request] == "GROUPFILE"):
						userInGroupMenu = True
						allFileAutMod = self._database.fileAuthoriseToModify(self._username)
						print(allFileAutMod)
						fileNum = 0
						for i in allFileAutMod:

							self.send((str(fileNum) + ") " + str(i)))
							fileNum+=1
						self.send("FINISHED")
						self.send(str(fileNum-1))
						while(userInGroupMenu):
							userRequest = self.receive()
							if (self._macrosDictionnary[userRequest] !="QUIT"):
								fileToShare = self.receive()
								userToShareWith = self.receive()
								if(self._database.userNameExist(userToShareWith) and self._username!=userToShareWith):
									self.send(self.getMacroValue("USERNAMEOK"))
									permissions = self.receive()
									groupAgree = self.receive()
									if(groupAgree=='y'):
										groupAgree=1
										self._database.putDisagree(self._username,allFileAutMod[int(fileToShare)])
									else:
										groupAgree=0
									self._database.insertShareFile(userToShareWith,allFileAutMod[int(fileToShare)],permissions,groupAgree)
									self._database.commit()
									print("User added")
								else:
									self.send(self.getMacroValue("USERNAMENOTOK"))
							else:
								userInGroupMenu = False

					elif(self._macrosDictionnary[request] == "VALIDATEUSERS"):	
						print("the client requested the list of users to validate")
						allUsers = self._database.userToAccept()
						for i in allUsers:
							self.send(i)
						self.send("FINISHED")	
						usersValidated=self.receive()
						usersValidatedList = usersValidated.split(",")
						if (len(usersValidatedList)>1):
							for i in usersValidatedList:
								self._database.acceptUser(i)
						else:
							self._database.acceptUser(usersValidated)		
						self._database.commit()	
				else:
					print("this request code doesn't exist..")
		except socket.error as e:
			self.handleKeyboardInterrupt(e,False)


	def getFileName(self, path):
		listing = path.split("/")
		return listing[len(listing)-1]

	def receiveDirectory(self, path = ""):
		directoryToCreate = self.receive()
		self._database.insertFileCreation(self._username,directoryToCreate,"w",self._username+"/"+path+directoryToCreate)

		try:
			os.mkdir(self._username+"/"+path+directoryToCreate)
		except OSError as e:
			print("your directory already exists!")
		notFinished = False
		while (not notFinished):
			finishOrNot = self.receive()
			if (finishOrNot=="CONTINUE"):
				response=self.receive()
				if (response=="DIRECTORY"):
					print("received DIRECTORY=======")
					newPath=self.receive()
					print("received currentpath======")
					return self.receiveDirectory(newPath)	
				elif(response=="NOTDIRECTORY"):
					print("received NOTDIRECTORY=======")
					newPath=self.receive()
					print("received currentpath======")
					self.receiveFile(newPath)
			elif (finishOrNot=="NOTCONTINUE"):
				notFinished = True
				print("receive NOT CONTINUE")
				return True	
		print("apres le while------")			

	def sendDirectory(self, fileName, path):
		print("envoie d'un répertoire")
		print("nom du repertoire: "+fileName)
		self.send(fileName)
		print("sent FILENAME=====")
		listOfFiles = os.listdir(path)
		for file in listOfFiles:
			self.send("CONTINUE")
			print("SENT CONTINUE====")
			if (os.path.isdir(path+"/"+file)):
				self.send("DIRECTORY")
				print("sent DIRECTORY=======")
				self.send(self.getCurrentPath())
				print("sent CURRENTPATH=====")
				self._listPathRecursive.append(file)
				self.sendDirectory(file,path+"/"+file)
			else:
				self.send("NOTDIRECTORY")
				print("sent NOTDIRECTORY=======")
				self.send(self.getCurrentPath())
				print("sent CURRENTPATH=====")
				self._listPathRecursive.append(file)
				print("send file")
				self.sendFile(file, path)

			self._listPathRecursive.pop()					

	def getCurrentPath(self):
		currentpath = ""
		for i in range(len(self._listPathRecursive)):
			if (i!=len(self._listPathRecursive)):
				currentpath+=self._listPathRecursive[i]+"/"
			else:
				currentpath+=self._listPathRecursive[i]	
		return currentpath


	def receiveFile(self, path, singleFile = False):
		print("Waiting upload from the client")
		try:
			file = self.receive()
			print(file)
			receivingFile = True
			if (not singleFile):
				fullPath = self._username+"/"+path+file
			else:
				fullPath = path+"/"+file
			print(fullPath)		
			self._database.insertFileCreation(self._username,file,"w",fullPath)
			with open(fullPath, 'wb') as fileToWrite:
				while receivingFile:
					data = self.receive(True)
					if(data==b' '):
						receivingFile = False
						fileToWrite.close()
					else:
						fileToWrite.write(data)
		except socket.error as e:
			self.handleKeyboardInterrupt(e,False)

	def sendFile(self,fileName,whereItIsStore, singleFile = False):
		try:
			self.send(fileName) # Nom du fichier
			print("sent file name: "+fileName)
			if (not singleFile):
				file = whereItIsStore+"/"+fileName
			else:
				file = whereItIsStore
			with open(file, 'rb') as fileToSend:
				for data in fileToSend:
					print(data)
					self.send(data, True)
				fileToSend.close()
			self.send(' ')
			print("fin envoie file")

		except socket.error as e:
			self.handleKeyboardInterrupt(e,False)

if __name__ == "__main__" : 
	if(len(sys.argv)==3):
		server = Server(str(sys.argv[1]),str(sys.argv[2]))
		
	else:
		print("Not enough argument given. Please type your mysql Username and password as arguments.")
