CREATE DATABASE NasDB;
USE NasDB CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE User (
	Username varchar(255) UNIQUE NOT NULL,
	Password varchar(255) NOT NULL,
	SecretPass varchar(255),
	Accepted BOOLEAN DEFAULT false,
	Administrator BOOLEAN DEFAULT false,
	PRIMARY KEY(Username)
);

CREATE TABLE File (
	Name varchar(255) DEFAULT NULL,
	RealPath varchar(255) UNIQUE NOT NULL primary key
);

CREATE TABLE Rights (
	Username varchar(255) references User(Username),
	FilePath varchar(255) references File(Name),
	rights varchar(255) NOT NULL,
	Agreements BOOLEAN DEFAULT NULL
);


INSERT INTO User(Username,Password,SecretPass,Accepted,Administrator) VALUES("toto","$2b$12$302KnhCTFA/oAqVHHaPrPesZaZXK4h3hCGkf2DXXFyoe3bBMg.GBC","toto",true,true);
