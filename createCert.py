from OpenSSL import crypto, SSL
from socket import gethostname
from pprint import pprint
from time import gmtime, mktime
from os.path import exists, join

CERT_FILE = "certification.pem"
KEY_FILE = "myapp.key"

def create_self_signed_cert(cert_dir):
    """
    If datacard.crt and datacard.key don't exist in cert_dir, create a new
    self-signed cert and keypair and write them into that directory.
    """

    if not exists(join(cert_dir, CERT_FILE)) \
            or not exists(join(cert_dir, KEY_FILE)):

        # create a key pair
        k = crypto.PKey()
        k.generate_key(crypto.TYPE_RSA, 1024)

        # create a self-signed cert
        cert = crypto.X509()
        cert.get_subject().C = "BE"
        cert.get_subject().ST = "Zaventem"
        cert.get_subject().L = "Vlaams"
        cert.get_subject().O = "SecureSoftware"
        cert.get_subject().OU = "JuToTed"
        cert.get_subject().CN = "SECU"
        cert.set_serial_number(1000)
        cert.gmtime_adj_notBefore(0)
        cert.gmtime_adj_notAfter(60*60*24*30)
        cert.set_issuer(cert.get_subject())
        cert.set_pubkey(k)
        cert.sign(k, "sha256")

        pub = crypto.dump_certificate(crypto.FILETYPE_PEM, cert)
        priv = crypto.dump_privatekey(crypto.FILETYPE_PEM, k)
        open(join(cert_dir, CERT_FILE), "wt").write(pub.decode("utf-8")
            )
        open(join(cert_dir, KEY_FILE), "wt").write(priv.decode("utf-8")
            )
        
create_self_signed_cert(".")